# Ansible Role for Service freeradius on Linux

This is a Ansible Role for Linux OS mainly Ubuntu Installing and Configure freeradius (https://freeradius.org/). The Role is tested against a CI/CD Pipeline in Gitlab and is prepared to deploy it in a CICD Pipeline . 

Requirements:
    None

Role Variables:
    See in the defaults Directory

Example Playbook:
```YAML
- hosts: all
  gather_facts: yes
  roles:
     - ansible-role-freeradius
```

Stages / Tags for CI/CD Deployment
- build
- prepare
- deploy
- verify

Testing
- install molecule
```
 4536  sudo pip install virtualenv==20.6.0
 4537  virtualenv --python 3 --system-site-packages virtenv
 4538  source virtenv/bin/activate
 4539  pip3 install setuptools docker apt-wrapper molecule==3.0.5 molecule-docker
```

- molecule --version
- molecule list
- molecule matrix test
- molecule check
- molecule test

Tested:
 - Vagrant Ubuntu 18.04
 - docker Ubuntu 18.04
 - docker Ubuntu 20.04
 
License:
    MIT / BSD

Author Information:
roland@stumpner.at